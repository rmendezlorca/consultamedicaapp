import { ConsultaService } from './../../services/consulta.service';
import { ActivatedRoute } from '@angular/router';
import { Consulta } from './../../model/consulta';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {

  cantidad:number;
  dataSource:MatTableDataSource<Consulta>;
  displayedColumns=['id','especialista','paciente','acciones'];
  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(public route:ActivatedRoute,private consultaService:ConsultaService,private snackBar:MatSnackBar) { }

  ngOnInit() {
    this.consultaService.consultaCambio.subscribe(data => {
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
    this.consultaService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data,'Aviso',{duration:2000});
    });
    this.consultaService.listar().subscribe(data=>{
      this.dataSource=new MatTableDataSource(data);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
    
  }
  applyFilter(filterValue:string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter=filterValue;
  }
  eliminar(id:number){
    this.consultaService.eliminar(id).subscribe(data => {
      this.consultaService.listar().subscribe(data =>{
        this.consultaService.consultaCambio.next(data);
        this.consultaService.mensajeCambio.next('Se elimino');
      });
    });
  }

}
