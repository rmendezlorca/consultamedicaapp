export class Paciente{
    apellidos : string
    direccion : string
    email : string
    id : number
    nombres : string
    run : string
    telefono : string
}