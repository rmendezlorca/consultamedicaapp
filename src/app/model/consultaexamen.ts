import { Consulta } from './consulta';
import { Examen } from './examen';
export class ConsultaExamen{
    consulta=new Consulta();
    examen=new Examen();
}
export class ConsultaListaExamen{
    consulta:Consulta
    lstExamen:Examen[];
}