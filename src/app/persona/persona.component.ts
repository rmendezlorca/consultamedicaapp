import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent implements OnInit {
  nombre:string;
  apodo:string;
  personas:string[]=[];
  constructor() { }

  ngOnInit() {
    this.nombre="Mello";
    this.apodo="";
  }
  escribir( e:any){    
    let texto=e.target.value;
    console.log(texto);
    this.nombre=texto;  

  }
  agregar(){
    this.personas.push(this.apodo);
    console.log(this.personas);

  }

}
