import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HOST, TOKEN_NAME } from './../shared/var.constant';
import { Consulta } from './../model/consulta';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConsultaService {
  consultaCambio=new Subject<Consulta[]>();
  mensajeCambio=new Subject<string>();
  url:string=`${HOST}/consultas`;
  constructor(private http:HttpClient) { }
  listar(){    
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Consulta[]>(this.url, 
      {
        headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });

  }
  modificar(consulta:Consulta){    
   
    return this.http.put(this.url,consulta);
  }
  registar(consulta:Consulta){
    
    return this.http.post(this.url,consulta);
  }
  listarPorId(id:number){
    
    return this.http.get<Consulta>(`${this.url}/${id}`);    
  }
  eliminar(id:number){    
    return this.http.delete(`${this.url}/${id}`);
  }
}
