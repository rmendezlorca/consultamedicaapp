import { HttpClient } from '@angular/common/http';
import { HOST } from './../shared/var.constant';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Medico } from '../model/especialista';

@Injectable({
  providedIn: 'root'
})
export class EspecialistaService {

  medicoCambio=new Subject<Medico[]>();
  mensajeCambio=new Subject<string>();
  url:string=`${HOST}/medicos`;
  constructor(private http:HttpClient) { }
  listar(){    
    return this.http.get<Medico[]>(this.url);

  }
  modificar(medico:Medico){    
    console.log(this.url);
    console.log(medico);
    return this.http.put(this.url,medico);
  }
  registar(medico:Medico){
    
    return this.http.post(this.url,medico);
  }
  listarPorId(id:number){
    
    return this.http.get<Medico>(`${this.url}/${id}`);    
  }
  eliminar(id:number){    
    return this.http.delete(`${this.url}/${id}`);
  }
}
