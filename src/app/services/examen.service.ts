import { HOST, TOKEN_NAME } from './../shared/var.constant';
import { Injectable } from '@angular/core';
import { Examen } from '../model/examen';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExamenService {
  examenCambio=new Subject<Examen[]>();
  mensajeCambio=new Subject<string>();
  url:string=`${HOST}/examenes`;
  constructor(private http:HttpClient) { }
  listar(){  
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;  
    return this.http.get<Examen[]>(this.url,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
  });

  }
  modificar(examen:Examen){    
    console.log(this.url);
    console.log(examen);
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;  
    return this.http.put(this.url,examen,{
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')});
  }
  registar(examen:Examen){
    
    return this.http.post(this.url,examen);
  }
  listarPorId(id:number){
    
    return this.http.get<Examen>(`${this.url}/${id}`);    
  }
  eliminar(id:number){    
    return this.http.delete(`${this.url}/${id}`);
  }
}