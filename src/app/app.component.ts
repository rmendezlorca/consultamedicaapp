import { LoginService } from './services/login.service';
import { MenuService } from 'src/app/services/menu.service';
import { Component } from '@angular/core';
import { Menu } from './model/menu';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Consulta';
  menus:Menu[]=[]
  constructor(private menuService:MenuService,public loginService:LoginService){}
  ngOnInit(){
    this.menuService.menuCambio.subscribe(data=>{
      this.menus=data;
    });
  }
}
